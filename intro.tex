\documentclass[tg.tex]{subfiles}
\begin{document}

\chapter{Introdução}\label{cap:intro}

\section{O processo de Migração Sísmica}

Empresas petrolíficas, como a brasileira Petrobrás, buscam constantemente novos reservatórios de petróleo. Para tal, essas empresas realizam missões de coleta de dados para determinar a composição de regiões subterrâneas/submarinas. Em áreas marítimas, essas missões necessitam de um navio que dispara ondas sonoras em sequência para o fundo do mar e através de vários sensores medem o tempo
de trânsito de tais ondas. Tais dados permitem inferir as transições entre regiões do subsolo e características destas regiões ao variarmos a profundidade analisada. Para tal, faz-se necessário um processamento delicado desses dados, composto por dezenas de procedimentos. Dentre tais procedimentos, podemos destacar o processo de Migração Sísmica, o qual domina o tempo de execução.

Atualmente, a única maneira de ter certeza que uma área possui petróleo é realizar uma missão de perfuração, a qual possui um custo muito elevado. Se for possível prever com alta confiabilidade se uma área possui ou não petróleo, pode-se eliminar a necessidade de tais missões para áreas não promissoras. Por isso, o processo de Migração Sísmica é extremamente relevante na busca de novos reservatórios de petróleo. Esse processo é demorado e os algoritmos utilizados no mesmo costumam demandar uma alta capacidade de computação. Costuma-se paralelizar a execução desses algoritmos em vários núcleos a fim de atingir a capacidade necessária para executá-los num intervalo de tempo razoável para sua aplicação.

O objetivo do processo de Migração Sísmica é de verificar se uma dada região tridimensional do subterrâneo possui um suposto conjunto de transições utilizando os dados coletados como entrada. A saída final dos algoritmos usados nesse processo é uma matriz tridimensional (profundidade, coordenadas na superfície) de densidades em cada ponto, a qual é utilizada para efetuar tal verificação. Dentre os vários algoritmos para efetuar tal processo, destaca-se o algoritmo OmegaXY, que embora tenha um alto custo computacional, possui resultados de alta qualidade.

Continuando o trabalho da referência bibliográfica [1], o qual estudou formas de paralelização de algoritmo OmegaXY em vários núcleos, o trabalho atual valida algumas conclusões feitas naquele e investiga as causas de certos fenômenos identificados. O trabalho mencionado será denominado TGALEX durante o restante deste trabalho.

\section{O algoritmo OmegaXY}\label{sec:algo_omegaxy}

O algoritmo OmegaXY possui um custo computacional bastante elevado, mas a qualidade dos seus resultados o torna bastante atraente para validar resultados de algoritmos mais baratos. Por exemplo, pode-se utilizar algoritmos mais baratos computacionalmente em várias áreas e ao se encontrar uma área promissora, o algoritmo OmegaXY pode ser utilizado para confirmar os resultados dos mais baratos.

A entrada principal desse algoritmo é um conjunto de matrizes com dimensões $x$ e $y$ (coordenadas na superfície) em função de instantes de tempo (t), $M_{in}(t)$, que representam os dados coletados numa área retângular da superfície em uma determinada missão. Esse conjunto de matrizes e outros parâmetros de entrada será denominado modelo no restante desse trabalho. A saída desse algoritmo é um conjunto de matrizes com dimensões $x$ e $y$ em função da profundidade (z), $M_{out}(z)$.

A parte central do algoritmo trabalha com matrizes de números complexos de dimensões $x$ e $y$ em função de frequência ($w$) e profundidade ($z$), $M_c(w,z)$. Em um primeiro passo do algoritmo, aplica-se uma Transformada Discreta de Fourier, que transforma as matrizes $M_{in}(t)$ nas matrizes que representam a profundidade inicial (z=1, a superfície), $M_c(w,1)$.

\textbf{Observação sobre unidades:} Toda menção feita neste trabalho à frequência $w$ se refere na verdade a um índice que identifica a frequência real. Ou seja, a variável frequência $w$ utilizada neste trabalho é um número inteiro adimensional que identifica alguma frequência resultante do processo da Transformada de Fourier Discreta aplicada a entrada. O mesmo vale para as variáveis profundidade ($z$) e coordenadas na superfície ($x$ e $y$), as quais são números inteiros adimensionais que representam uma profundidade/coordenada real.

Para se obter a matriz $M_c(w,z)$, o algoritmo OmegaXY faz um processo denominado extrapolação, utilizando como base $M_c(w,z-1)$. Ou seja:

\begin{equation}M_c(w,z) = f_{extrap}(M_c(w,z-1))\label{eq:extrap}\end{equation}

Isso significa que existe uma dependência de dados em nosso domínio e isso influencia diretamente estratégias de paralelização desse algoritmo, como será abordado posteriormente.

Para obter a saída para uma profundidade, a matriz $M_{out}(z)$, é necessário combinar as matrizes
para cada frequência da profundidade $z$, $M_c(w,z)$. Denomina-se esse processo de redução e, no caso do algoritmo OmegaXY, trata-se de uma simples soma das partes reais, como mostra a equação \ref{eq:reduce}. Foge ao escopo deste trabalho explicar o motivo para tal fato, mas é necessário conhecê-lo para entender o estudo feito.

\begin{equation}M_{out}(z) = \sum^{}_{w}{Re(M_c(w,z))}\label{eq:reduce}\end{equation}

Uma maneira simples de implementar esse algoritmo pode ser resumida pelo seguinte pseudocódigo considerando que todos os dados podem ser guardados em memória. Esta idéia de implementação do algoritmo será chamada de estratégia sequencial e mais elaborada dela será mostrada na Secção \ref{sec:estrat_seq}.

\begin{Listings}{lst:pseudocod_seq_simples}{Pseudocódigo para estratégia sequencial considerando que todos os dados podem ser guardados em memória}
double Min(1...Nt)[Nx][Ny]
complex M_c(1...Nw,1...Nz)[Nx][Ny]
double Mout(1...Nz)[Nx][Ny]
# Realizar Transformada de Fourier Discreta em Min e obter M_c(iw, 1) para cada frequencia iw
# Zerar matriz de saida
para cada profundidade iz (2, 3, ...)
	para cada frequencia iw (1, 2, ...)
		// Extrapolacao
		Mc(iw, iz) = fextrap(Mc(iw, iz-1))
		// Reducao incremental
		Mout(z) += Re(M_c(iw, iz))
\end{Listings}

\textbf{Observação sobre pseudocódigos:} Ao longo deste trabalho, será convencionado que linhas começando com `//' são comentários sobre a linha abaixo, linhas começando com `\#' descrevem um procedimento alto nível e linhas que não comecem com `//' ou `\#' são operações expressas matematicamente, loops, declarações de memória, etc.

\section{Motivação}\label{sec:motivacao}

Uma óbvia estratégia de paralelização para esse algoritmo é alocar para cada núcleo um conjunto de frequências e fazer com que cada núcleo processe todas as profundidades para tal conjunto, gerando resultados parciais de $Mout(z)$, visto que para se calcular $M_c(w, z)$ só é necessário se ter $Mc(w, z-1)$, ou seja, todas as profundidades de uma determinada frequência podem ser processadas independentemente dos dados de outras frequências. Seria necessário fazer um processo de redução final para combinar os resultados parciais, que pode ser feito tanto no final do processo quanto ao final de processamento de uma profundidade. Esta estratégia e outras serão melhor explicadas e ilustradas no Capítulo \ref{cap:review}.

A estratégia de paralelização por frequência tem a grande vantagem de tornar os núcleos quase independentes na maior parte do tempo, mas vale notar que o tempo necessário para efetuar a operação de redução aumenta com o aumento do número de processadores. O TGALEX relata a suspeita de que essa operação de redução pode se tornar um gargalo para essa estratégia e limitar sua aplicação para um número muito grande de núcleos. Por isso, foram propostas no TGALEX duas estratégias de paralelização alternativas. Uma delas, é alocar um conjunto de profundidades para os processadores e realizar uma operação de redução incremental. A outra é apenas uma variação da estratégia por profundidades, que foi denominada de estratégia mista.

Teoricamente essas estratégias alternativas deveriam escalar melhor com o aumento do número de processadores, como será melhor explicado no Capítulo \ref{cap:review}. Entretanto, os testes feitos no TGALEX revelaram justamente o contrário, negando assim a expectativa inicial. Vale ressaltar que os testes do TGALEX foram feitos em um cluster não homogêneo (com nós de computação de características diferentes) e alguns resultados indicam que os testes não foram feitos sobre o mesmo tipo de nó. Isso motivou uma revalidação desses resultados em nós homogêneos, que será apresentada no trabalho atual.

Ao se repetir os testes do TGALEX, foi possível perceber que os resultados qualitativos se mantinham. A estratégia de paralelização por profundidade era a pior em termos de speedup e a por frequência era a melhor. Vale notar que mesmo a estratégia por frequência atingiu um speedup longe do ideal nos testes do atual trabalho. Nos testes com 16 núcleos, o paralelismo por frequência obteu um speedup por volta de 11 e o por profundidade um speedup por volta de 9. A imperfeição do speedup do paralelismo por frequência e o desempenho pobre apresentado pelo paralelismo por profundidade relativo ao da frequência motivaram uma investigação mais profunda das causas destes fatos.

\section{Objetivos}

\begin{enumerate}
	\item Verificar se os resultados do TGALEX se mantém numa máquina homogênea.
	\item Explicar speedups imperfeitos na estratégia de paralelismo por frequência.
	\item Explicar os motivos para a estratégia de paralelismo por profundidade ser pior do que a por frequência.
\end{enumerate}

\section{Estrutura do trabalho}

No Capítulo \ref{cap:intro}, foi dada uma introdução ao assunto deste trabalho, assim como a motivação, os objetivos e a estrutura do trabalho.

No Capítulo \ref{cap:parallel_info}, alguns conceitos sobre processamento paralelo serão apresentados e será feita uma breve introdução sobre Message Passing Interface (MPI), a tecnologia usada para implementação paralela dos algoritmos.

No Capítulo \ref{cap:review}, as propostas e alguns resultados chave do TGALEX serão apresentados. Esse capítulo deve resumir o necessário do TGALEX para que se possa entender o trabalho atual.

No Capítulo \ref{cap:verify_results}, os resultados do TGALEX serão verificados em uma máquina diferente e serão tecidos comentários sobre a significância dos mesmos.

No Capítulo \ref{cap:measuring_time}, serão explicados os conceitos básicos sobre instrumentação e a biblioteca auxiliar desenvolvida para tal. Também serão mostrados alguns fenômenos descobertos.

No Capítulo \ref{cap:investigating}, os speedups imperfeitos na estratégia de paralelismo por frequência serão explicados através de um dos fenômenos descobertos pela instrumentação.

No Capítulo \ref{cap:modelagem}, será feita uma modelagem do paralelismo por profundidade a luz dos fenômenos apresentados no Capítulo \ref{cap:measuring_time}.

No Capítulo \ref{cap:conclusao}, serão apresentadas as conclusões deste trabalho e sugestões de trabalhos futuros na área.

\end{document}
