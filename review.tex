\documentclass[tg.tex]{subfiles}
\begin{document}

\chapter{Revisita ao TGALEX}\label{cap:review}

\section{Modelos utilizados}

Um modelo, como definido na Secção \ref{sec:algo_omegaxy}, é o conjunto de dados de entrada para o algoritmo OmegaXY. Para o teste das implementações do OmegaXY, foram obtidos 12 modelos de entrada, os quais são identificados por números entre 1 a 64 que os identificam.

O tamanho dos modelos é determinado com base em alguns parâmetros que fazem parte do próprio modelo, que identificam o tamanho dos vetores/matrizes de entrada. Os modelos possuem três dimensões espaciais, \emph{nx}, \emph{ny} e \emph{nz}, além de dimensões de vetores de frequência \emph{nw} e dimensão de vetores de tempo, \emph{nt}. A Tabela \ref{tab:dim_modelos} mostra os parâmetros/dimensões dos modelos utilizados.

\begin{LongTable}{|r|r|r|r|r|r|}{tab:dim_modelos}{
	Parâmetros/dimensões dos modelos utilizados
}{
	\hline
	\multicolumn{1}{|c|}{Modelo}	&
	\multicolumn{1}{c|}{nx}	&
	\multicolumn{1}{c|}{ny}	&
	\multicolumn{1}{c|}{nz}	&
	\multicolumn{1}{c|}{nw}	&
	\multicolumn{1}{c|}{nt}
	\\ \hline
}
1	&	64	&	64	&	25	&	32	&	32 \\ \hline
2	&	64	&	64	&	25	&	32	&	32 \\ \hline
3	&	161	&	161	&	100	&	128	&	128 \\ \hline
4	&	161	&	161	&	100	&	128	&	128 \\ \hline
5	&	256	&	256	&	150	&	256	&	256 \\ \hline
6	&	256	&	256	&	150	&	256	&	256 \\ \hline
12	&	201	&	201	&	150	&	128	&	128 \\ \hline
51	&	256	&	256	&	150	&	256	&	256 \\ \hline
61	&	256	&	128	&	1500	&	4096	&	3000 \\ \hline
62	&	256	&	128	&	150	&	256	&	256 \\ \hline
63	&	512	&	128	&	150	&	256	&	256 \\ \hline
64	&	512	&	128	&	150	&	512	&	512 \\ \hline
\end{LongTable}

\section{Estratégia sequencial}\label{sec:estrat_seq}

Na Listagem \ref{lst:pseudocod_seq_simples} foi mostrado um pseudocódigo para a estratégia sequencial (sem paralelização) considerando que todos os dados podem ser colocados na memória principal. Entretanto, quando as dimensões do modelo crescem, isto não é possível e há necessidade de salvar resultados intermediários no disco.

Para evitar manter todos os dados intermediários em memória, pode-se usar um arquivo intermediário que guarda apenas $M_c(iw, iz)$ para a última profundidade $iz$ calculada/obtida. A Listagem \ref{lst:pseudocod_seq}, extraida do TGALEX e ligeiramente modificada neste trabalho, mostra um pseudocódigo dessa forma de implementar a estratégia sequencial.

\begin{Listings}{lst:pseudocod_seq}{Pseudocódigo para estratégia sequencial utilizando arquivos intermediários}
double dat[Nx][Ny];
complex cdat[Nx][Ny];
# Realizar Transformada de Fourier Discreta em Min e obter M_c(iw, 1) para cada frequencia iw
# Guardar todos os M_c(iw, 1) no arquivo A, sendo cada M_c(iw, 1) guardado na posicao iw
para cada profundidade iz (2, 3, ...)
	// Zerar matriz dat
	dat = 0
	para cada frequencia iw (1, 2, ...)
		# Ler a posicao iw do arquivo A e guardar em cdat
		// Extrapolacao
		cdat = fextrap(cdat)
		# Salvar cdat na posicao iw do arquivo A
		// Reducao incremental
		dat += Re(M_c(iw, iz))
	# Salvar dat no arquivo de saida na posicao iz
\end{Listings}

\textbf{Notação:} Ao longo deste trabalho, os tempos gastos com as operações necessárias para calcular e usar $M_c(w,z)$ serão qualificadas por par $<w, z>$. Por exemplo, o tempo total gasto em um par $<w, z>$ se refere ao tempo gasto na iteração do algoritmo onde se calcula $M_c(w,z)$, incluindo tempos para ler arquivos, enviar mensagens, realizar reduções, etc. Na Listagem \ref{lst:pseudocod_seq}, o tempo total gasto em um par $<w, z>$ se refere ao tempo gasto nas linhas 6 a 14 na iteração correspondente ao par. Outro exemplo é o tempo de computação de um par $<w, z>$, o qual se refere ao tempo gasto exclusivamente aplicando $f_{extrap}$ para calcular $M_c(w,z)$. No caso da Listagem \ref{lst:pseudocod_seq}, o tempo de computação de um par $<w, z>$ corresponde ao tempo gasto na linha 11 na iteração correspondente ao par.

A Figura \ref{fig:ilustracao_estrat_seq} ilustra a maneira que a estratégia sequencial percorre o domínio. Nela, as linhas representam as diferentes profundidades e as colunas as diferentes frequências. A seta indica a ordem com que o domínio é percorrido. Os círculos vermelhos representam operações de I/O (acesso ao disco).

\begin{Figure}{fig:ilustracao_estrat_seq}{
	Ilustração da estratégia sequencial
}
\includegraphics[height=13cm]{images/ilustracao_estrat_seq.png}
\end{Figure}

\newpage
\section{Estratégia de paralelização por frequências}

Como comentado na Secção \ref{sec:motivacao}, uma estratégia de paralelização natural, visto as dependências de dados do problema, seria atribuir a cada núcleo um conjunto de frequências e deixá-lo processar todas as profundidades daquele conjunto em particular. A Listagem \ref{lst:pseudocod_freq} mostra o pseudocódigo dessa estratégia.

\begin{Listings}{lst:pseudocod_freq}{Pseudocódigo para estratégia de paralelização por frequência}
double dat[Nx][Ny];
double fdat[Nx][Ny];
complex cdat[Nx][Ny];
// ID do Processo MPI
int este_proc;

// Processo Mestre
se este_proc == 0
	# Realizar Transformada de Fourier Discreta em Min e obter M_c(iw, 1) para cada frequencia iw
	# Guardar todos os M_c(iw, 1) no arquivo A, sendo cada M_c(iw, 1) guardado na posicao iw
	# Guardar matriz Nx por Ny zerada no arquivo B[este_proc] na posicao iz para cada profundidade iz
	# Mandar sinal liberando outros processos
// Outros processos
caso contrario
	# Esperar sinal para comecar

para cada frequencia iw alocada para este processo
	# Ler a posicao iw do arquivo A e guardar em cdat
	para cada profundidade iz (2, 3, ...)
		// Extrapolacao
		cdat = fextrap(cdat)
		# Ler a posicao iz do arquivo B[este_proc] e guardar em dat
		// Reducao
		dat += Re(M_c(iw, iz))
		# Salvar dat na posicao iz do arquivo B

para cada profundidade iz (1, 2, 3, ...)
	# Ler a posicao iz do arquivo B[este_proc] e guardar em dat
	# Operacao de reducao entre todos os processos
	fdat = Reduce(dat)
	se este_proc == 0
		# Salvar fdat no arquivo de saida na posicao iz
\end{Listings}

Um detalhe interessante da Listagem \ref{lst:pseudocod_freq} que vale ser ressaltado é que, durante o loop principal onde ocorre a extrapolação, cada processo escreve no seu próprio arquivo intermediário, o qual foi denotado no pseudocódigo de \code{B[este\_proc]}.

A alocação de frequências poderia ser feita de várias formas. A forma escolhida no TGALEX foi atribuir uma faixa de frequências consecutivas para cada processo. A Figura \ref{fig:ilustracao_estrat_freq} ilustra como o domínio foi dividido e percorrido. Cada cor representa um processo MPI e as frequências são coloridas conforme a alocação feita.

\begin{Figure}{fig:ilustracao_estrat_freq}{
	Ilustração da estratégia de paralelização por frequência
}
\includegraphics[height=13cm]{images/ilustracao_estrat_freq.png}
\end{Figure}

\section{Estratégia de paralelização por profundidades}\label{sec:estrat_prof}

Uma maneira alternativa de se dividir o domínio, apresentada no TGALEX, é atribuir a cada processador um conjunto de profundidades. Por exemplo, sendo $N_{procs} = 4$, o processo 0 se responsibilizaria pelas as profundidades 2,6,10..., já o processo 1 se responsibilizaria por 3,7,11,... . A vantagem de se fazer isso é que a operação de redução pode ser eliminada, visto que o processo que realiza uma profundidade $z$ pode simplesmente acumular em memória o resultado final para aquela profundidade, de maneira semelhante a mostrada no pseudocódigo da estratégia sequencial, vide Listagem \ref{lst:pseudocod_seq}. Assim, essa estratégia deveria escalar melhor do que a estratégia de paralelização por frequência com o aumento do número de núcleos.

Um problema existente nesta estratégia é a necessidade de se ter disponível $M_c(w,z-1)$ para poder calcular $M_c(w,z)$. Para resolver esse problema introduzido pela dependência de dados, pode-se utilizar a técnica de pipeline, onde o processo responsável por uma profundidade $z-1$ manda uma mensagem para o para o processo responsável pela profundidade $z$ contendo $M_c(w,z-1)$ para cada frequência $w$ processada. O processo responsável pela profundidade $z$, deverá então esperar essa mensagem para que possa começar.

A Figura \ref{fig:ilustracao_estrat_prof} mostra como esta estratégia percorre o domínio e facilita a visualização do pipeline. Em tal figura, $N_{procs} = 4$. O processo 0 é alocado com a profundidade $z=2$ e para cada frequência $w$ lê do disco os dados iniciais ($M_c(w,1)$). Ao finalizar o cálculo de $M_c(w,2)$ para um determinado $w$, o processo 0 manda uma mensagem para o processo 1, o qual inicia o processamento $M_c(w,3)$. O processo 1 realiza um prodecimento semelhante, só que ao invés de ler do disco, tal processo utiliza os dados recebidos através de mensagens do processo 1. Os processos 2 e 3 são análogos ao processo 1. Entretanto, note que se o número de frequências a serem processadas for suficientemente grande, o processo 0 não estará pronto para receber uma mensagem do processo 3, visto que o processo 0 não terá acabado a profundidade 2. Por isso, há necessidade do processo 3 salvar no disco o que tiver calculado para que posteriormente o processo 0 possa fazer os cálculos para profundidade 6. Este fenônemo será denominado wrap-around.

\begin{Figure}{fig:ilustracao_estrat_prof}{
	Ilustração da estratégia de paralelização por profundidade
}
\includegraphics[height=13cm]{images/ilustracao_estrat_prof.png}
\end{Figure}

A Listagem \ref{lst:pseudocod_prof} mostra o pseudocódigo desta estratégia.

\begin{Listings}{lst:pseudocod_prof}{Pseudocódigo para estratégia de paralelização por profundidade}
double dat[Nx][Ny];
complex cdat[Nx][Ny];
// ID do Processo MPI
int este_proc;

// Processo Mestre
se este_proc == 0
	# Realizar Transformada de Fourier Discreta em Min e obter M_c(iw, 1) para cada frequencia iw
	# Guardar todos os M_c(iw, 1) no arquivo A, sendo cada M_c(iw, 1) guardado na posicao iw


para cada profundidade iz alocada para este processo
	// Zerar matriz dat
	dat = 0
	para cada frequencia iw (1, 2, ...)
		se este_proc == 0
			# Ler a posicao iw do arquivo A e guardar em cdat
		caso contrario
			# Esperar receber mensagem contendo Mc(iw, iz-1) e guardar em cdat
		// Extrapolacao
		cdat = fextrap(cdat)
		// Reducao incremental
		dat += Re(M_c(iw, iz))
		se este_proc == Nprocs-1
			# Escrever na posicao iw do arquivo A e guardar em cdat
		caso contrario
			# Enviar mensagem com o conteudo de cdat, ou seja, Mc(iw, iz)
	# Salvar dat no arquivo de saida na posicao iz
\end{Listings}

Uma vantagem desta estratégia é que ela utiliza menos operações de I/O do que a estratégia por frequência. Operações de I/O podem demorar consideravelmente dependendo da máquina utilizada e das dimensões do modelo. Entretanto, uma desvantagem é que o acesso ao disco é efetuado apenas pelo processo 0, o que significa que se essas operações demorarem, o pipeline pode ficar desbalanceado. Vale lembrar que uma característica de um bom pipeline se encontra na homogêneidade de cada um de seus estágios, visto que se um estágio for mais demorado que os outros ele irá atrasar o pipeline inteiro.

\section{Estratégia de paralelização mista}

Como visto na Secção \ref{sec:estrat_prof}, o processo 0 é o único processo que faz I/O na estratégia de paralelização por profundidade e isso pode causar problemas de desempenho no pipeline. Por isso, foi proposto no TGALEX uma alternativa que foi denominada de paralelização mista.

Considere novamente a situação mostrada na Figura \ref{fig:ilustracao_estrat_prof}, a qual mostra a divisão de domínio do paralelismo por profundidades. A estratégia mista é parecida, com a única diferença sendo que no momento em que deveria acontecer o wrap-around, no qual o processo 3 seria obrigado a salvar no disco e o processo 0 continuaria processando a profundidade pela qual era responsável. Em tal momento, o processo 0 grava o $Mc_{4, 2}$ em disco e pula para iniciar a profundidade 6, recebendo então uma mensagem do processo 3. Esta situação é mostrada na Figura \ref{fig:ilustracao_estrat_mista}. Todos os outros processos se comportam então de maneira análoga ao processo 0, indo ao disco a cada 4 frequências processadas.

\begin{Figure}{fig:ilustracao_estrat_mista}{
	Ilustração da estratégia de paralelização por profundidade
}
\includegraphics[height=13cm]{images/ilustracao_estrat_mista.png}
\end{Figure}

Existem alguns detalhes quanto a alocação, pois ela não é tão trivial quanto a das outras estratégias, especialmente no caso do número de profundidades a serem processadas não ser divisível pelo número de núcleos. Tais detalhes e o próprio pseudocódigo serão omitdos neste trabalho, visto que a estratégia mista não foi alvo de estudo neste trabalho e seus detalhes são bem explicados pelo TGALEX.

\end{document}
