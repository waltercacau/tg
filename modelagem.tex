\documentclass[tg.tex]{subfiles}
\begin{document}

\chapter{Modelagem analítica para o paralelismo por profundidades}\label{cap:modelagem}

\section{Tempos de comunicação}\label{sec:comunicacao}

Para analisar a fundo o desempenho da estratégia de paralelismo por profundidades, viu-se necessário também analisar o tempo médio que leva uma para uma mensagem MPI contendo uma matriz com os dados calculados para um par$<w,z>$ ser enviada. Esse tempo será chamado de Tempo de Comunicação ($T_{c}$).

Com o objetivo de medir esse tempo, foi criada uma barreira MPI com o objetivo de sincronizar os processos. Depois de sincronizados, cada processo $p$ emitia uma mensagem de log indicando o instante de tempo em relação a sua referência de quando a sincronização aconteceu ($t_{sync}(p)$). Posteriormente, para cada evento de enviar ou receber uma mensagem MPI $m$, uma mensagem de log era emitida contendo o instante de tempo em relação a sua referência ($t_{send}(m,p)$ ou $t_{recv}(m,p)$). Posteriormente, calculamos o tempo que levou a transmissão de uma mensagem MPI $m$ do processo $p_a$ para o processo $p_b$, $t_{msg}(m,p_a,p_b)$, através da seguinte equação:

\begin{equation}
t_{msg}(m,p_a,p_b) = (t_{recv}(m,p_b)-t_{sync}(p_b))-(t_{send}(m,p_a)-t_{sync}(p_a))
\end{equation}

Viu-se necessário medir esse tempo individualmente para várias combinações de modelos assim e número de núcleos utilizados, uma vez que esses fatores influenciam no tamanho das mensagens, na quantidade e, consequentemente, possível congestionamento delas. Sendo assim, foram medidos esses tempos para os modelos 3 e 64, utilizando 2, 4, 8, 16 e 24 núcleos.

A Tabela \ref{tab:tempos_medios_comunicacao} mostra os tempos médios gastos para realizar trocas de mensagens entre processos.

\begin{LongTable}{|r|r|r|r|r|r|r|r|r|}{tab:tempos_medios_comunicacao}{
	Tempos médios gastos para realizar trocas de mensagens entre processos.
}{
	\hline
	\multicolumn{1}{|c|}{\multirow{2}{*}{Modelo}}	&
	\multicolumn{8}{c|}{Tempos(ms)}
	\\ \cline{2-6}
	&
	\multicolumn{1}{c|}{2 núcleos}	&
	\multicolumn{1}{c|}{4 núcleos}	&
	\multicolumn{1}{c|}{8 núcleos}	&
	\multicolumn{1}{c|}{16 núcleos}	&
	\multicolumn{1}{c|}{24 núcleos}	\\ \hline
}
3			&	14.08	&	24.70	&	32.00	&	22.33	&	47.14	\\ \hline
64 			&	15.53	&	30.91	&	37.45	&	42.48	&	44.45	\\ \hline
\end{LongTable}

A Tabela \ref{tab:distribuicao_mensagens} mostra a distribuição das mensagens do modelo 64, na execução com 24 núcleos.

\begin{LongTable}{|r|r|r|r|r|r|r|r|r|}{tab:distribuicao_mensagens}{
	Distribuição das mensagens do modelo 64, na execução com 24 núcleos.
}{
	\hline
	\multicolumn{1}{|c|}{Intervalo de durações (ms)}	&
	\multicolumn{1}{c|}{Número de mensagens observadas} \\ \hline
}
0 a 1		&	1568	\\ \hline
1 a 10		&	306		\\ \hline
10 a 30		&	11123	\\ \hline
30 a 50		&	14242	\\ \hline
50 a 70		&	5001	\\ \hline
70 a 100	&	3560	\\ \hline
100 a 500	&	462		\\ \hline
500	a 1000	&	45		\\ \hline
> 1000		&	45		\\ \hline
\end{LongTable}

A variação dos tempos exibidos na Tabela \ref{tab:tempos_medios_comunicacao} e instabilidade dos tempos exibida na Tabela \ref{tab:distribuicao_mensagens} mostra que está havendo congestionamento ou alguma outra irregularidade na troca de mensagens. Na situação de congestionamento, o aumento do número de núcleos implicaria no aumento do congestionamento e consequentemente no aumento do tempo médio levado para trocar mensagens. Esse comportamento pode ser bem observado, exceto pela execução do modelo 3 com 16 núcleos.

Nota-se, ao se comparar a Tabela \ref{tab:comp_time_values_mod3} e a Tabela \ref{tab:tempos_medios_comunicacao}, que os tempos médios de comunicação encontrados para o Modelo 3 são comparáveis ao tempo de processamento de um par$<w,z>$. O mesmo aparenta ser válido para os outros modelos pelos experimentos feitos neste trabalho. Possivelmente esses tempos de comunicação elevados são parte da causa do pobre desempenho da estratégia de paralelismo por profundidade em relação às outras.

\section{Premissas do modelamento}

De forma a tentar concluir o porquê do desempenho ruim da estratégia por profundidades, foi realizado um modelamento analítico de seu tempo de execução de forma a tentar estimar seu tempo de execução total real, que será testado com certas combinações de modelos e de número de núcleos. Para isso, foram consideradas algumas premissas e simplificações.

Conforme observou-se experimentalmente (vide Tabela \ref{tab:comp_time_values_mod3}) e com a análise do código fonte do programa, concluímos que o tempo de computação de cada par $<w, z>$ ($T_{w,z}$) varia significantemente apenas com a freqüência do par, mas não varia praticamente nada com mudança de profundidades, quando mantém-se a freqüência. Ou seja,

\begin{equation}
T_{w,z} \cong T_{w}
\end{equation}

A Tabela \ref{tab:definicao_tempos_parwz} utiliza essa propriedade para gerar uma imagem da distribuição dos tempos de computação.

\begin{LongTable}{|r|r|r|r|r|r|r|r|r|}{tab:definicao_tempos_parwz}{
	Tempos de computação em cada par <w, z>
	}{
	\hline
	\multicolumn{1}{|c|}{\multirow{2}{*}{Profundidades}}	&
	\multicolumn{7}{c|}{Frequências}

	\\ \cline{2-8}
	&
	\multicolumn{1}{c|}{$w = 1$}	&
	\multicolumn{1}{c|}{$w = 2$}	&
	\multicolumn{1}{c|}{$w = 3$}	&
	\multicolumn{1}{c|}{...}	&
	\multicolumn{1}{c|}{$w = j$}	&
	\multicolumn{1}{c|}{...}	&
	\multicolumn{1}{c|}{$w = N_{w}$}	\\ \hline
}
$z = 1$		&	$T_{1}$	&	$T_{2}$	&	$T_{3}$	&	...		&	$T_{j}$	&	...		&	$T_{N_{w}}$	\\ \hline
$z = 2$		&	$T_{1}$	&	$T_{2}$	&	$T_{3}$	&	...		&	$T_{j}$	&	...		&	$T_{N_{w}}$ \\ \hline
$z = 3$		&	$T_{1}$	&	$T_{2}$	&	$T_{3}$	&	...		&	$T_{j}$	&	...		&	$T_{N_{w}}$ \\ \hline
...			&	...		&	...		&	...		&	...		&	...		&	...		&	...		 	\\ \hline
$z = i$		&	$T_{1}$	&	$T_{2}$	&	$T_{3}$	&	...		&	$T_{j}$	&	...		&	$T_{N_{w}}$ \\ \hline
...			&	...  	&	...		&	...		&	...		&	...		&	...		&	...		    \\ \hline
$z = N_{z}$	&	$T_{1}$	&	$T_{2}$	&	$T_{3}$	&	...		&	$T_{j}$	&	...		&	$T_{N_{w}}$ \\ \hline
\end{LongTable}

Com o que observado nas medições do Capítulo 5, percebeu-se que, devido a correção de Li (que representa o maior custo de processamento de cada par<w,z>), conforme a freqüência aumenta, há  um aumento do tempo de execução. Temos então que

\begin{equation}
T_{i} \leq T_{j}, \text{para }i < j
\end{equation}

No paralelismo por profundidade (em que todos os pares de uma profundidade são processados por um mesmo processador), temos a dependência de que para iniciar o processamento de cada par$<w,z>$ é necessário que o par$<w,z-1>$ já tenha sido processado e os dados resultantes desse processamento enviados para o processo que irá processar o par (vamos considerar o tempo de envio desse sinal constante e igual a $T_{c}$).

\begin{equation}
 \text{Tempo de comunicação } T_{c} \text{ constante}
\end{equation}

Além disso, para não complicar mais a modelagem e delimitar as causas e efeitos dos problemas no pipeline da estratégia de paralelismo por profundidades, considera-se apenas um número pequeno de profundidades de forma que não ocorra wrap-around. Ou seja, há um núcleo disponível e dedicado para processar cada produnfidade.

\begin{equation}
 \text{Número de Núcleos } = N_{z} \text{(Número de profundidades)}
\end{equation}

\section{Modelagem do Tempo de Espera}

Com as premissas estabelecidas, podemos modelar o tempo de espera $E_{w,z}$ gasto em cada par$<w,z>$, que vamos definir como o tempo que o processo passa aguardando até iniciar o processamento do par$<w,z>$ após ter terminado o processamento do par$<w-1,z>$.  Temos então que

\begin{equation}
E_{1,z}=(z-1)* (T_{1}+T_{c}), \text{ trivial: tempo inicial de espera de cada processo (*)}
\end{equation}

\begin{equation}
E_{w,z}=(z-1)*(T_{w}-T_{w-1}), \text{ para $w\neq1$ (**)}
\end{equation}

(*): Analisando a execução dos pares da primeira freqüência: O processo da primeira profundidade não espera ninguém para iniciar a primeira freqüência. O da segunda profundidade precisa esperar o da primeira acabar mais o tempo de comunicação $(T_{1}+T_{c})$, o da terceira profundidade precisa esperar o da segunda acabar mais outro tempo de comunicação, ao todo $2(T_{1}+T_{c})$, e assim em diante.

(**): Para demonstrar a expressão para o tempo de pares com a freqüência de índice diferente de 1, vamos utilizar a seguinte indução:

1) Caso inicial: Na primeira profundidade em que $z=1$ temos que o tempos de espera $E_{w,1}$ são todos iguais a zero, pois não há nenhuma dependência para o início do processamento de cada par. Quando o processo termina o par$<w,1>$, ele inicia imediatamente o processamento do par$<w+1,1>$:

\begin{equation}
E_{w,1}=0 ,\text{ para todo } w
\end{equation}

2) Passo de indução: dada a hipótese que  $E_{w,z}=(z-1)*(T_{w}-T_{w-1})$, vamos calcular $E_{w,z+1}$. Para mostrar esse passo, vamos considerar o momento em que o processo encarregado da profundidade $z$ ($P_{z}$) termina de processar o par$<w-1,z>$ e envia o sinal de comunicação $S_{1}$ para o processo encarregado da profundidade $z+1$ ($P_{z+1}$). O diagrama da Figura \ref{fig:diagrama_demosntracao_modelagem_by_depth} visa facilitar o entendimento e a ordem cronológica dos eventos envolvidos.

\begin{Figure}{fig:diagrama_demosntracao_modelagem_by_depth}{Diagrama de temporização para analisar o tempo de espera Ew,z+1.}
\includegraphics[height=3cm]{images/diagrama_demonstracao_modelagem_by_depth.png}
\end{Figure}

Explicação dos instantes no diagrama da Figura \ref{fig:diagrama_demosntracao_modelagem_by_depth}:

$t(1)$: $P_{z}$ termina de processar o par$<w-1,z>$ e envia o sinal $S_{1}$ com os dados resultantes para $P_{z+1}$. A partir daí $P_{z}$ fica aguardando até poder iniciar o processamento do par$<w,z>$.

$t(2)$: $P_{z+1}$ recebe o sinal $S_{1}$ e dá início ao processamento do par$<w-1,z+1>$.

$t(3)$: $P_{z}$ termina a espera, recebendo um sinal com os dados necessários para dar início ao processamento do par$<w,z>$.

$t(4)$: $P_{z+1}$ termina de processar o par$<w-1,z+1>$ e começa a esperar pelo sinal $S_{2}$ (Esse é o intervalo de tempo $E_{w,z+1}$ que queremos calcular).

$t(5)$: $P_{z}$ termina de processar o par$<w,z>$ e envia o sinal $S_{2}$ para $P_{z+1}$.

$t(6)$: $P_{z+1}$ recebe o sinal $S_{2}$ e com isso terminar o período de espera $E_{w,z+1}$.

Vamos analisar a diferença entre os instantes $t(6) - t(2)$ em $P_{z}$ e $P_{z+1}$

\begin{equation}
t(6)-t(2)=(E_{w,z}-T_{c}+T_{w}+T_{c})=(T_{w-1}+E_{w,z+1})
\end{equation}
\begin{equation}
E_{w,z+1}= (E_{w,z}+ T_{w})-T_{w-1}=(z-1)*(T_{w}-T_{w-1})+ T_{w}-T_{w-1}=z*(T_{w}-T_{w-1})
\end{equation}
\begin{equation}
E_{w,z+1}=((z+1)-1)*(T_{w}-T_{w-1}) , \text{ como queriamos mostrar. }
\end{equation}

\section{Tempo total de execução do programa}

Com isso, podemos estimar o tempo total da execução dessa forma de paralelismo, que é o maior tempo de execução individual de um processo encarregado por uma profundidade. Sabemos que o processo de maior tempo total será o encarregado pela profundidade $N_{z}$ (pois ele depende de todos os outros para terminar). Sendo assim, temos que o tempo total de execução desse processo é

\begin{equation}
Total(P_{N_{z}})=\sum_{i=1}^{N_{w}}(E_{i,Nz}+T_{i})
\end{equation}
\begin{equation}
Total(P_{N_{z}})=(N_{z}-1)*[T_{c}+(T_{1})+(T_{2}-T_{1})+ ... +(T_{N_{w}}-T_{N_{w}-1})]+\sum_{i=1}^{N_{w}}(T_{i})
\end{equation}
\begin{equation}\label{eqn:estimar_temp_exec_prof}
Total(P_{N_{z}})=(N_{z}-1)*[T_{c}+T_{N_{w}}]+\sum_{i=1}^{N_{w}}(T_{i})
\end{equation}

\section{Estimativas do tempo total de execução e comparação com medições}

Pode-se utilizar essa Fórmula \ref{eqn:estimar_temp_exec_prof} para estimar o tempo de execução da estratégia de profundidade a partir de um tempo de comunicação conhecido e de tempos de execução do processamento de pares$<w,z>$ medidos na execução do programa sequêncial. Porém, como já foi observado, o fato do programa ser executado em paralelo gera concorrência por recursos e consequentemente faz com o que o tempo de processamento de um par seja maior do que no programa sequencial. Com o intuito de corrigir esse efeito, os tempos medidos no programa sequencial serão multiplicados por um fator de correção ($Fator_{norm,cores}$) considerando o número de núcleos utilizados, obtido através do tempo médio normalizado para o processamento de um par$<w,z>$ . Foi realizada uma estimativa para os modelos 3 e 64, com 24 profundidades ambos, utilizando 24 núcleos, considerando os resultados observados anteriormente neste trabalho (vide tabelas \ref{tab:tempos_medios_comunicacao} e \ref{tab:tempos_norm_seq_paralelizado}):

\begin{equation}
T_{c}=45ms \text{ e } Fator_{norm,24} = 1.260
\end{equation}

\begin{equation}
T_{N_{w}} \cong T_{N_{w},seq} * Fator_{norm,24}
\end{equation}

\begin{equation}
\sum_{i=1}^{N_{w}}(T_{i}) \cong \sum_{i=1}^{N_{w}}(T_{i,seq}) * Fator_{norm,24}
\end{equation}

A Tabela \ref{tab:tempos_utilizados_estimativa} mostra alguns detalhes para o cálculo das estimativas e a Tabela \ref{tab:estimativas_tempo_total} compara o tempo estimado com o tempo medido.

\begin{LongTable}{|r|r|r|r|r|r|r|r|r|}{tab:tempos_utilizados_estimativa}{
	Tempos utilizados para realizar a estimativa do tempo total de execução.
}{
	\hline
	\multicolumn{1}{|c|}{Modelo}	&
	\multicolumn{1}{c|}{$T_{N_{w},seq}$ (ms)}	&
	\multicolumn{1}{c|}{$\sum_{i=1}^{N_{w}}(T_{i,seq})$ (ms)}	&
	\multicolumn{1}{c|}{$T_{N_{w}}$ (ms)}	&
	\multicolumn{1}{c|}{$\sum_{i=1}^{N_{w}}(T_{i})$ (ms)}	\\ \hline

}
3[$N_{z}=24$]			&	76.61	&	4078.87		&	96.52	&	5139.37	 \\ \hline
64[$N_{z}=24$] 			&	70.72	&	16760.52	&	89.10	&	21118.25 \\ \hline
\end{LongTable}

\begin{LongTable}{|r|r|r|r|r|r|r|r|r|}{tab:estimativas_tempo_total}{
	Estimativas do tempo de execução da estratégia de paralelismo por profundidades.
}{
	\hline
	\multicolumn{1}{|c|}{Modelo}	&
	\multicolumn{1}{c|}{Tempo estimado (s)}	&
	\multicolumn{1}{c|}{Tempo medido (s)}	&
	\multicolumn{1}{c|}{Erro \%}	 \\ \hline

}
3[$N_{z}=24$]			&	8.39	&	8.62	&	2.6		\\ \hline
64[$N_{z}=24$]			&	24.20	&	29.77	&	18.7	\\ \hline
\end{LongTable}

O erro maior observado na estimativa de tempo total do modelo 64 pode ser explicado pelo seu pipeline mais longo (no modelo 64 tem-se que $N_{w} = 512$, enquanto que no modelo 3, $N_{w}=128$), mais susceptível às irregularidades do tempo de comunicação, como podem ser observadas na Tabela \ref{tab:distribuicao_mensagens}. O modelamento realizado considera $T_{c}$ constante, um suposição que talvez não seja muito adequada, dada a dispersão desses tempos de comunicação em valores que as vezes chegam a durar mais de 1 segundo.

\end{document}
