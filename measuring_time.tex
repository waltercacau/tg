\documentclass[tg.tex]{subfiles}
\begin{document}

\chapter{Instrumentação para medição detalhada do tempo de execução}\label{cap:measuring_time}

\section{A biblioteca de instrumentação desenvolvida}

Para investigar os motivos por trás dos desempenhos observados na Secção \ref{sec:verify_results_summary}, era necessário medir o tempo gasto em certas etapas do algoritmo OmegaXY. Para tal, as implementações das estratégias foram instrumentadas, ou seja, foram adicionadas chamadas a funções especiais em localizações de interesse do código para se medir o tempo de execução gasto em determinados trechos. Tais funções especiais foram encapsuladas em uma pequena biblioteca desenvolvida no decorrer deste trabalho. Nesta secção, os conceitos por trás dessa biblioteca serão explicados.

A saída do processo de instrumentação é um conjunto de linhas contendo informações sobre a execução do código. Cada linha é denominada de mensagem de log. Certas funções da biblioteca geram tais mensagens.

Existem dois tipos de mensagens de log. O primeito tipo, TIMEDLOG, é uma mensagem de log que possui um número inteiro em nanosegundos que indica o instante de tempo no qual aquela mensagem foi gerada em relação a uma referência a priori desconhecida. Dentro de um mesmo processo, essa referência é fixada como sendo o instante em que a primeira mensagem de log do tipo TIMEDLOG foi gerada. O segundo tipo, LOG, é uma mensagem de log sem o instante de referência na qual foi gerada.

Cada mensagem de log possui uma lista de informações coletadas, as quais serão denominadas comandos de log. Posteriormente, as mensagens de log são interpretadas e processadas para extrair tais informações em um formato mais útil. O nome dado à suite de programas que realiza tal processamento é DataMiner.

\newpage
Resumidamente, uma mensagem de log contém nesta ordem:

\begin{enumerate}
	\item ID do processo segundo o sistema operacional
	\item Instante de tempo se for do tipo TIMEDLOG
	\item Lista de comandos de log
	\item Arquivo e linha no arquivo onde a mensagem de log foi gerada.
\end{enumerate}

A Listagem \ref{lst:log_format} exemplifica o formato das mensagens de log geradas.

\begin{Listings}{lst:log_format}{Exemplo do formato das mensagens geradas pela biblioteca de instrumentação desenvolvida}
TIMEDLOG(14799,0ns): ENTER_SCOPE(MAIN);[main_by_depth_mpi.c/12]
LOG(14799): GLOBAL_PROP(mpi_proc_id,0);[main_by_depth_mpi.c/48]
...
TIMEDLOG(14799,30138781ns): ENTER_SCOPE(MIOLO);[miolo_by_depth_mpi.c/77]
TIMEDLOG(14799,30383506ns): ENTER_SCOPE(IO);[miolo_by_depth_mpi.c/133]
...
TIMEDLOG(14799,36328084ns): LEAVE_SCOPE(IO);[miolo_by_depth_mpi.c/133]
...
TIMEDLOG(14799,190622778ns): LEAVE_SCOPE(MIOLO);[miolo_by_depth_mpi.c/281]
\end{Listings}

Formalmente, uma mensagem de log pode ser descrita através de uma gramática livre de contexto, como mostrado na Listagem \ref{lst:log_grammar}. A notação utilizada para descrever tal gramática é a Augmented Backus–Naur Form, descrita na RFC5234. A única diferença é que os literais são case-sensitive, ou seja, `ABC' se refere a string com todos os caracteres maiúsculos.

\newpage
\begin{Listings}{lst:log_grammar}{Gramática Livre de Contexto de uma mensagem de log}
inicio          = ( timedlogmsg / logmsg )
timedlogmsg     = "TIMEDLOG(" id_do_processo "," instante "ns):" [comandos] origem
logmsg          = "LOG(" id_do_processo "):" comandos origem
comandos        = comando [comandos]
comando         = " " nome_de_comando "(" [argumentos] ");"
argumentos      = argumento ["," argumentos]
origem          = "[" nome_de_arquivo "/" numero_da_linha "]"
inteiro         = <numero inteiro>
id_do_processo  = inteiro
numero_da_linha = inteiro
instante        = inteiro
argumento       = <qualquer sequencia de caracteres que nao contenha "," ou ")">
nome_de_comando = <qualquer sequencia de caracteres que nao contenha "(">
nome_de_arquivo = <qualquer sequencia de caracteres que nao contenha "]" ou "/">
\end{Listings}

Outro conceito importante embutido na biblioteca de instrumentação desenvolvida é o conceito de escopo. Escopos são trechos de código que se deseja medir a duração. Um exemplo de possível escopo de interesse deste trabalho é o trecho que contenha a chamada a função $f_{extrap}$ para que se possa determinar quanto tempo se gasta no algoritmo com a extrapolação. Outro exemplo de escopo seria o trecho interno do loop de profundidades na estratégia sequencial, visto que pode ser interessante saber quanto tempo em média se gasta para processar uma profundidade inteira nesta estratégia.

Assim como todo trecho de código não atômico (não composto por uma única instrução / comando) pode ser dividido em subtrechos, é natural que um escopo possa ser dividido em subescopos, ou seja, um escopo pode conter outros escopos. A utilidade de um escopo poder conter outros escopos é fácil de entender, visto que mesmo medindo a duração de um certo trecho de código, pode ser necessário detalhar quais subtrechos gastam mais tempo.

\newpage
Escopos tem tipos, que servem basicamente para facilitar sua caracterização. Exemplo de tipos importantes de escopos usados são:
\begin{enumerate}
	\item \code{COMPUTATION}: Delimita o trecho de código de aplicação da $f_{extrap}$ no algoritmo OmegaXY. Ou seja, esse escopo delimita as instruções responsáveis pela extrapolação.
	\item \code{IO}: Delimita operações de acesso ao disco.
	\item \code{COMMUNICATION}: Delimita chamadas a funções MPI para envio ou recebimento de mensagens.
	\item \code{PROF\_FREQ}: Delimita todas as operações necessárias para o cálculo de um par <w, z>.
\end{enumerate}

Escopos são delimitados por comandos de log \code{ENTER\_SCOPE} e \code{LEAVE\_SCOPE}. Ao ler as mensagens de log e interpretar os comandos, o DataMiner mantém uma pilha com todos os escopos ainda abertos, ou seja, escopos que foram declarados pelo comando \code{ENTER\_SCOPE}, mas não foram fechados pelo comando \code{LEAVE\_SCOPE} . A Listagem \ref{lst:log_format} mostra a abertura do escopo MAIN, sucedido pela abertura do escopo MIOLO e posteriormente do escopo IO. Note que descobrir o tempo gasto em um escopo é uma simples questão de subtrair o instante da mensagem de log que contém o comando \code{LEAVE\_SCOPE} pelo instante da mensagem que contém o \code{ENTER\_SCOPE} correspondente. Por exemplo, o tempo gasto no escopo de IO da Listagem \ref{lst:log_format} é 36328084-30383506=5944578ns=5.9ms .


Um compromisso por organização foi feito na implementação do DadaMiner. Apesar de que teoricamente nada impede que o usuário da biblioteca feche um escopo antes dos subescopos terem sido fechados, isso complicaria demasiadamente o código sem agregar muita utilidade. A Listagem \ref{lst:scope_inconsistency_example} mostra um exemplo de tal situação. Por isso, o DataMiner emite erros ao encontrar situações como esta, informando inconsistência. Logo, para a atual implementação do DataMiner, escopos são consistentes se e somente se a seguinte afirmação for verdadeira: se um escopo X tiver intersecção com outro escopo Y, ou X está contido em Y ou Y está contido em X. Note que esta restrição implica em uma estrutura hieráquica entre escopos, ou seja, tem-se árvores de escopos. Outra restrição feita pelo DataMiner é que cada processo contenha um escopo (geralmente do tipo MAIN) que contém todos os outros escopos do programa. O motivo para tal restrição é evitar que existam várias árvores de escopo em um mesmo processo.

\begin{Listings}{lst:scope_inconsistency_example}{Exemplo de escopos inconsistentes. MIOLO é fechado antes do subescopo IO}
...
TIMEDLOG(14799,30138781ns): ENTER_SCOPE(MIOLO);[miolo_by_depth_mpi.c/77]
TIMEDLOG(14799,30383506ns): ENTER_SCOPE(IO);[miolo_by_depth_mpi.c/133]
TIMEDLOG(14799,33355795ns): LEAVE_SCOPE(MIOLO);[miolo_by_depth_mpi.c/281]
TIMEDLOG(14799,36328084ns): LEAVE_SCOPE(IO);[miolo_by_depth_mpi.c/133]
...
\end{Listings}

Também é possível associar uma propriedade a um escopo. Uma propriedade é uma variável que qualifica o escopo. Para tal, o comando \code{SCOPE\_PROP} é usado. Ele recebe três argumentos: o tipo do escopo, o nome da propriedade e o valor. A utilidade disso é caracterizar os escopos para fazer alguma estatística. A seguir será mostrada uma situação que exemplifica a necessidade deste comando.

Considere a implementação instrumentada simplista do algoritmo OmegaXY sequencial em C mostrada na Listagem \ref{lst:instrumented_code_example}, equivalente ao pseudocódigo mostrado na Listagem \ref{lst:pseudocod_seq_simples}. Esta implementação exemplifica o uso das chamadas da biblioteca. \code{\_TIMEDLOG\_} e \code{\_LOG\_} geram respectivamente mensagens do tipo TIMEDLOG e de tipo LOG com os comandos delimitados pelos parênteses.

\newpage
\begin{Listings}{lst:instrumented_code_example}{Implementação instrumentada simplista do algoritmo OmegaXY sequencial em C}
double Min[Nt][Nx][Ny];
complex M_c[Nw][Nx][Ny];
double Mout[Nz][Nx][Ny];
// Realizar Transformada de Fourier Discreta em Min e obter M_c(iw, 1) para cada frequencia iw
// e zerar matriz de saida
// ...
_TIMEDLOG_(ENTER_SCOPE(MAIN));
for(int iz=1;iz<Nz;iz++) {
	_TIMEDLOG_(ENTER_SCOPE(PROF_SCOPE));
	_LOG_(SCOPE_PROP(PROF_SCOPE, "iz", iz));
	for(int iw=0;iw<Nw;iw++) {
		_TIMEDLOG_(ENTER_SCOPE(PROF_FREQ_SCOPE));
		_LOG_(SCOPE_PROP(PROF_FREQ_SCOPE, "iw", iw));
		_TIMEDLOG_(ENTER_SCOPE(COMPUTATION_SCOPE));
		// Extrapolacao: Usa o primeiro argumento como entrada e guarda resultado no segundo
		fextrap(Mc[iw][iz-1], Mc[iw][iz]);
		_TIMEDLOG_(LEAVE_SCOPE(COMPUTATION_SCOPE));
		// Reducao incremental
		Mout[iz] += Re(M_c[iw][iz]);
		_TIMEDLOG_(LEAVE_SCOPE(PROF_FREQ_SCOPE));
	}
	_TIMEDLOG_(LEAVE_SCOPE(PROF_SCOPE));
}
_TIMEDLOG_(LEAVE_SCOPE(MAIN));
\end{Listings}

Suponha que se deseja saber como o tempo gasto para se fazer a extrapolação varia com a profundidade e frequência. Para tal, deve-se gerar uma tabela contendo o tempo gasto em função das variáveis mecionadas. Note que os nós do tipo \code{PROF\_SCOPE} possuem a propriedade $iz$ e os nós do tipo \code{PROF\_FREQ\_SCOPE} possuem a propriedade $iw$, vide Figura \ref{fig:instrumented_code_example_scope_tree}. Ao percorrer a árvore procurando pelos nós do tipo \code{COMPUTATION\_SCOPE}, basta guardar os valores encontrados de $iz$ e $iw$ para posteriormente formar a tabela desejada.

\begin{Figure}{fig:instrumented_code_example_scope_tree}{Exemplo de árvore de escopos gerada pela Listagem \ref{lst:instrumented_code_example}}
\includegraphics[height=7cm]{images/instrumented_code_example_scope_tree.png}
\end{Figure}


\section{Resultados preliminares da instrumentação}

Esta secção se destina a demonstrar a existência de certos fenômenos, os quais foram descobertos através da análise das mensagens de log geradas pela biblioteca de instrumentação.

A partir da instrumentação, foi possível gerar uma imagem onde a cor de cada pixel representa os tempos gastos sobre algum critério (total, só computação, só IO, ...) para cada par $<w,z>$ de frequência e profundidade. Neste trabalho, tal imagem será chamada de Timemap e será qualificada com base no critério escolhido para gerá-la. Por exemplo, Timemap Total se refere a figura gerada utilizando os tempos dos escopos \code{PROF\_FREQ} para escolher a cor de cada pixel, visto que tal escopo engloba todo o trecho de código responsável pelo processamento de um par $<w,z>$, medindo assim o tempo total gasto com um par. Vale observar que o tempo total inclui tempos de espera pelos ou leitura dos dados de entrada. Timemap de Computação é análogo, só que ao invés de usar o escopo \code{PROF\_FREQ}, utiliza o escopo \code{COMPUTATION}.

A Figura \ref{fig:timemap_ref} ilustra o referencial do Timemap. Note que na coordenada vertical, crescente de cima para baixo, temos a variável profundidade, e na coordenada horizontal, crescente da esquerda para direita, temos a variável frequência.

\begin{Figure}{fig:timemap_ref}{Ilustração do referencial de um Timemap}
\includegraphics[height=7cm]{images/timemap_ref.png}
\end{Figure}

\begin{Figure}{fig:timemap_total_seq_mod3}{Timemap Total do Modelo 3 utilizando a estratégia sequencial}
\includegraphics[height=10cm]{images/timemap_total_seq_mod3.png}
\end{Figure}

A Figura \ref{fig:timemap_total_seq_mod3} mostra o Timemap Total gerado para o Modelo 3 utilizando a estratégia sequencial. Ao observar esta figura, nota-se uma propriedade muito interessante. Com o aumento da frequência, o tempo total de um par $<w,z>$ cresce, enquanto ao se mudar de profundidade o tempo total de um par é praticamente constante. Ao investigar o motivo para tal acontecimento, verificou-se que o culpado era o tempo de computação, que variava com a frequência, como pode ser observado no Timemap de Computação mostrado na Figura \ref{fig:timemap_comp_seq_mod3}. De fato, tal propriedade para o tempo de computação foi verificada para todas as outras estratégias utilizadas e para todos os modelos testados. Vale notar que a implementação da função $f_{extrap}$ é compartilhada por todas as estratégias de paralelização e pela própria estratégia sequencial. Note também que esta propriedade impacta diretamente qualquer estratégia baseada em pipeline, como, por exemplo, a estratégia em profundidade.

\begin{Figure}{fig:timemap_comp_seq_mod3}{Timemap de Computação do Modelo 3 utilizando a estratégia sequencial}
\includegraphics[height=10cm]{images/timemap_comp_seq_mod3.png}
\end{Figure}

A Figura \ref{fig:avg_comp_time_per_freq} exemplifica como o tempo de computação de um par $<w, z>$ varia com a frequência. Note a não linearidade do gráfico e a variação considerável no tempo médio entre a menor e a maior frequência.

\begin{Figure}{fig:avg_comp_time_per_freq}{Tempo médio de computação de um par $<w, z>$ para o Modelo 3 em função da frequência}
\includegraphics[height=9cm]{images/avg_comp_time_per_freq.png}
\end{Figure}

Para demonstrar a constância do tempo de computação quando se mantém a frequência constante e varia a profunidade, a Tabela \ref{tab:comp_time_values_mod3} foi confeccionada.

\begin{LongTable}{|r|r|r|r|r|}{tab:comp_time_values_mod3}{
	Dados sobre os tempos de computação para cada par $<w, z>$ no Modelo 3 variando número de núcleos e estratégias de implementação do OmegaXY.
}{
	\hline
	\multicolumn{1}{|c|}{Estratégia}	&
	\multicolumn{1}{|c|}{Nº de Núcleos}	&
	Média (ms) &
	\begin{minipage}[t]{3cm}\begin{center}
		Desvio Padrão Máximo encontrado para uma dada frequência (ms)
	\end{center}\end{minipage} &
	\begin{minipage}[t]{3cm}\begin{center}
		Razão Desvio Padrão por Média (\%)
	\end{center}\end{minipage}
	 \\ \hline
}

Sequencial		&	1	&	63.75	&	0.39	&	0.61 \\ \hline
Profundidade	&	4	&	75.04	&	2.90	&	3.86 \\ \hline
Frequência		&	4	&	75.20	&	1.22	&	1.62 \\ \hline
Mista			&	4	&	76.32	&	1.16	&	1.53 \\ \hline
Profundidade	&	8	&	80.48	&	3.10	&	3.85 \\ \hline
Frequência		&	8	&	81.47	&	1.43	&	1.75 \\ \hline
Mista			&	8	&	82.39	&	1.57	&	1.90 \\ \hline
Profundidade	&	16	&	80.67	&	2.98	&	3.69 \\ \hline
Frequência		&	16	&	81.57	&	2.39	&	2.93 \\ \hline
Mista			&	16	&	82.52	&	2.29	&	2.78 \\ \hline
\end{LongTable}

O sentido da coluna ``Desvio Padrão Máximo encontrado para uma dada frequência'' desta tabela é medir a constância do tempo de computação quando se mantém a frequência constante. Ou seja, tal coluna mede o desvio padrão máximo encontrado entre todos os tempos de computação de um par $<w, z>$ para uma frequência fixa. De fato, ao examinar a razão desta coluna com a coluna `Média', apresentada na coluna `Razão Desvio Padrão por Média', nota-se uma insignificante variação relativa dos tempos de computação ao se manter a frequência fixa.

A Tabela \ref{tab:comp_time_values_mod3} revela outro fato interessante. O tempo de computação gasto em um par $<w, z>$ varia com o número de processos utilizados. O que é bastante curioso sobre esse fato é que o código para realizar essa computação é compartilhado por todas as estratégias, ou seja, em todos estes testes o mesmo código foi executado utilizando um mesmo processador para uma mesma entrada e ainda assim houve diferença no tempo de execução. Provavelmente existe algum recurso compartilhado pelos núcleos que está sendo disputado pelos mesmos causando essa variação. Tal variação foi verificada para outros modelos também e será melhor estudada na Secção \ref{cap:investigating}.

\section{Resumo dos fenômenos descobertos}

Em resumo, foram descobertos os seguintes fenômenos através da instrumentação:

\begin{enumerate}
	\item O tempo de computação de um par $<w, z>$ cresce com o aumento da frequência mantendo a profundidade constante, mas é praticamente constante ao se variar a profundidade e manter a frequência constante.
	\item O tempo de computação de um mesmo par $<w, z>$ varia com o número de núcleos usados.
\end{enumerate}

\end{document}
